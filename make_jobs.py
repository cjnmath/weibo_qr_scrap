from rq import Queue
# from rq.decorators import job
from redis import Redis
from utiles.weibo_parser import get_follow_id_pair
from utiles.weibo_parser import get_page_id_oid
from time import sleep
from urllib.parse import urljoin

# start_url = "https://weibo.com/pengguanying"


redis_conn = Redis()
q = Queue(connection=redis_conn)
start_url = "https://weibo.com/u/6496148200"
# target_url = "https://weibo.com/zhongxilinpeng"
target_url = "https://weibo.com/seedkenjiwu"


def lpop_next_oid():
    result = redis_conn.lpop('waiting-list')
    if result is None:
        return result
    while redis_conn.sismember('scrapied-set', result):
        result = redis_conn.lpop('waiting-list')
    return result


def get_parent_oid(index_oid):
    index_level = int(redis_conn.hget('oid-level', index_oid))
    parent_set = redis_conn.smembers('{}-followed_by'.format(index_oid))
    r = parent_set
    while r:
        t = int(parent_set.pop())
        r = parent_set
        t_level = int(redis_conn.hget('oid-level', t))
        if t_level < index_level:
            return t
    return None


def print_follow_graph(index_oid):
    base_url = "https://weibo.com/u"
    result_links = [(str(index_oid), urljoin(base_url, str(index_oid)))]
    result = [str(index_oid)]
    parent_oid = get_parent_oid(index_oid)
    while parent_oid:
        result.append(str(parent_oid))
        result_links.append((str(parent_oid), urljoin(base_url, str(parent_oid))))
        parent_oid = get_parent_oid(parent_oid)
    print(' <== '.join(result))
    print(result_links)


# @job('high', connection=redis_conn, ttl=-1)
# def get_follow_graph(s_url, t_url):
    # q = Queue("low", connection=redis_conn)
start_page_id, start_oid = get_page_id_oid(start_url)
target_page_id, target_oid = get_page_id_oid(target_url)
oid = start_oid
while oid:
    job = q.enqueue(get_follow_id_pair, oid, result_ttl=600, job_id=str(oid))
    result = job.result
    n = 0
    while not result:
        sleep(5)
        result = job.result
        n += 1
        if n > 12:
            break
    redis_conn.sadd('scrapied-set', oid)
    redis_conn.hset('oid-level', oid, 1)
    found = False
    if result:
        for oid, follow_id in result:
            current_lever = int(redis_conn.hget('oid-level', oid))
            redis_conn.hset('oid-level', follow_id, current_lever+1)
            redis_conn.sadd('{}-followed_by'.format(follow_id), oid)
            redis_conn.rpush('waiting-list', follow_id)
            if follow_id == target_oid:
                print_follow_graph(follow_id)
                found = True
                break
    if found:
        q.delete(delete_jobs=True)
        break

    next_oid = redis_conn.lpop('waiting-list')
    oid = int(next_oid) if next_oid else None


# get_follow_graph.delay(start_url, target_url)
# q.enqueue_call(func=get_follow_graph, args=(start_url, target_url), ttl=-1)
