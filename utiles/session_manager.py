from .weibo import weibo_desktop
import pickle
import os
import time


script_dir = os.path.dirname(os.path.relpath(__file__))

pickle_preserved_time = 3600*8

session_dir = script_dir
file_path = os.path.join(session_dir, 'session.pickle')


def load_session():
    if not os.path.exists(file_path):
        print("[***] No session pickle file exists, require new...")
        make_new_session_pickle()
    if not is_pickle_fresh():
        print("[***] Session pickle is not fresh, require new...")
        make_new_session_pickle()
    with open(file_path, 'br') as f:
        session = pickle.load(f)
        print("[---] Session loaded!")
    return session


def pickle_produce_date():
    return os.path.getmtime(file_path)


def pickle_remaining_time():
    mdate = pickle_produce_date()
    pass_time = time.time() - mdate
    return pickle_preserved_time - pass_time


def is_pickle_fresh():
    if pickle_remaining_time() > 0:
        return True
    return False


def make_new_session_pickle(username=None, password=None):
    if (username, password) == (None, None):
        (username, password) = ("username", "passwd")
        print("[***] Using defaut user info...")
    else:
        print("[!!!] User info incomplete, abort!")
        return
    session = weibo_desktop(username, password).session
    print("[---] New login session got.")
    open(file_path, 'w').close()
    with open(file_path, 'ba') as f:
        pickle.dump(session, f)
    print("[---] Session pickle sucessfully made!")


def schedual_update_session(check_interval=3600):
    while True:
        if not is_pickle_fresh():
            make_new_session_pickle()
            print("[---] Update login session at {}".format(time.ctime(time.time())))
        time.sleep(check_interval)
