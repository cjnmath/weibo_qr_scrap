from .session_manager import load_session
import json
import re
from lxml.html import fromstring
import time


session = load_session()


def get_page_id_oid(url):
    response = session.get(url)
    page_id = re.search('CONFIG\[\'domain\'\]=\'(\d+)\'', response.text).group(1)
    oid = re.search('CONFIG\[\'oid\'\]=\'(\d+)\'', response.text).group(1)
    return page_id, oid


def get_page_id(oid):
    """
        oid := str or int
    """
    base_url = "https://weibo.com/u/{}".format(oid)
    response = session.get(base_url)
    page_id = re.search('CONFIG\[\'domain\'\]=\'(\d+)\'', response.text).group(1)
    return page_id


def get_follow_id_pair(oid, page_id=None):
    """
        oid := str or int
    """
    if not page_id:
        page_id = get_page_id(oid)
    base_url = "https://weibo.com/p/{}{}/follow".format(page_id, oid)
    response = session.get(base_url)
    pids = re.search('\"domid\"\:\"(Pl_Official_HisRelation__\d+)\"', response.text).group(1)
    results = []
    for i in range(1, 5):
        params = {
            'pids': pids,
            'page': i,
            'ajaxpagelet': 1,
            'ajaxpagelet_v6': 1,
            '_t': 'FM_{}'.format(int(time.time()*10**5))
        }
        response = session.get(base_url, params=params)
        print("[---] Get page {} for oid {}".format(i, oid))
        r = re.search('\((\{.*\})\)', response.text).group(1)
        d = json.loads(r)
        if d['html'].strip():
            root = fromstring(d['html'])
        else:
            print("[!!!] Nothing received, abort!")
            break
        raw_ids = root.xpath('//div[@class="follow_inner"]//li//a/@usercard')
        for raw_id in raw_ids:
            follow_id = re.search('id=(\d+)', raw_id).group(1)
            results.append((str(oid), follow_id))
    return results
