from redis import Redis
from rq import Queue


def add(a, b):
    return a + b


redis_conn = Redis()
q = Queue(connection=redis_conn)
jobs = []
for i in range(100000):
    job = q.enqueue(add, args=(i, i))
    jobs.append(job)
